import 'dart:async';
import 'dart:io';

import 'package:diy_api/diy_api.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:test/test.dart';


void main() async {
  group("Can refresh token successfully", () {
    final path = Uri.parse("https://test.dart/");
    Uri pathWith(String segment) => path.replace(path: segment);

    String serverToken = "";
    
    final dataResponse = Response("data", 200);
    final noTokenResponse = Response("noToken", 403);
    final service = HttpService(
      client: MockClient((request) async {
        switch ("${request.url}") {
          case "https://test.dart/refreshToken": return Response(serverToken, 200);
          case "https://test.dart/getData": {
            return request.headers[HttpHeaders.authorizationHeader] == serverToken
              ? dataResponse
              : noTokenResponse;
          }
          default: throw UnimplementedError("No endpoint for ${request.url}");
        }
      }),
    );

    service.headers[HttpHeaders.authorizationHeader] = "";
    setUp(() {
      service.headers[HttpHeaders.authorizationHeader] = "";
      serverToken = "";
    });

    test("getData with incorrect clientToken", () async {
      serverToken = "serverToken";
      service.headers[HttpHeaders.authorizationHeader] = "incorrectToken";

      final result = await service.send(
        method: Method.get,
        uri: pathWith("getData"),
      );
      final statusCode = result.mapOrElse((v) => v.statusCode, (e) => -1);
      expect(result.isOk, true);
      expect(statusCode, 403);
      expect(result.valueOrNull!.body, noTokenResponse.body);
    });
    
    test("getData with correct clientToken", () async {
      serverToken = service.headers[HttpHeaders.authorizationHeader] = "sameToken";

      final result = await service.send(
        method: Method.get,
        uri: pathWith("getData"),
      );
      final statusCode = result.mapOrElse((v) => v.statusCode, (e) => -1);

      expect(result.isOk, true);
      expect(statusCode, dataResponse.statusCode);
      expect(result.valueOrNull!.body, dataResponse.body);
    });
    
    test("getData with incorrect clientToken + interceptor to refresh token", () async {
      service.responseInterceptors.add(ResponseInterceptor(
        reactIf: (request, result) => result.mapOrElse(
          (value) => value.statusCode == 403,
          (error) => false,
        ),
        reaction: (request, result, http) => http
          .send(
            method: Method.post,
            uri: pathWith("refreshToken"),
          )
          .then((res) async => res.mapOrElse(
            (response) {
              service.headers[HttpHeaders.authorizationHeader] = response.body;
              return http.sendRequest(request);
            },
            (error) => Future.sync(() => res),
          )),
      ));
      serverToken = "serverToken";
      service.headers[HttpHeaders.authorizationHeader] = "incorrectToken";

      final result = await service.send(
        method: Method.get,
        uri: pathWith("getData"),
      );
      final statusCode = result.mapOrElse((v) => v.statusCode, (e) => -1);

      expect(result.isOk, true);
      expect(statusCode, dataResponse.statusCode);
      expect(result.valueOrNull!.body, dataResponse.body);
      expect(serverToken, service.headers[HttpHeaders.authorizationHeader]);
    });
  });
}
