import 'dart:typed_data';

import 'package:http/http.dart';

/// Convenience class for safely creating bodies for HTTP requests
/// 
/// To define custom bodies, use [CustomBody] instead. It allows the user to define
/// safe conversion from its content to one of the types required by [Request].
/// 
/// See also:
///  * [CustomBody], to define conversions for types other than [Map<String, String>],
///    [String] and [Uint8List]
///  * [HttpService], where Body is used
///  * [Request], for specifically the [Request.body], [Request.bodyBytes] and
///    [Request.bodyFields] setters.
abstract class Body {
  /// Creates a Body from [fields]
  const factory Body.fields(Map<String, String> fields) = _BodyFields;

  /// Creates a Body from [body]
  const factory Body.body(String body) = _BodyString;

  /// Creates a Body from [bytes]
  const factory Body.bytes(Uint8List bytes) = _BodyBytes;

  /// Creates a [CustomBody] with the given [converter]
  static CustomBody customFields<T>({
    required T content,
    required Map<String, String> Function(T) converter,
  }) => _CustomFieldsBody<T>(content, converter);

  /// Creates a [CustomBody] with the given [converter]
  static CustomBody customBody<T>({
    required T content,
    required String Function(T) converter,
  }) => _CustomStringBody<T>(content, converter);

  /// Creates a [CustomBody] with the given [converter]
  static CustomBody customBytes<T>({
    required T content,
    required Uint8List Function(T) converter,
  }) => _CustomBytesBody<T>(content, converter);

  const Body();

  /// Assigns the content wrapped by this instance to [request]
  void setRequestBody(final Request request);
}

class _BodyFields extends Body {
  const _BodyFields(this.fields);

  final Map<String, String> fields;

  void setRequestBody(final Request request) => request.bodyFields = fields;
}

class _BodyString extends Body {
  const _BodyString(this.body);

  final String body;

  void setRequestBody(final Request request) => request.body = body;
}

class _BodyBytes extends Body {
  const _BodyBytes(this.bytes);

  final Uint8List bytes;

  void setRequestBody(final Request request) => request.bodyBytes = bytes;
}

/// Convenience class for defining POD object convertible to [Request] bodies
abstract class CustomBody<Content, Target> extends Body {
  /// Defined to enable inheritance.
  /// 
  /// Usually the static methods [fields], [body] and [bytes] are enough.
  CustomBody(this.content, this.converter):
    assert(Target is Map<String, String> || Target is String || Target is Uint8List);

  /// Creates a CusomBody that will assign the result of `converter(content)` to
  /// [Request.bodyFields]
  static CustomBody fields<T>({
    required T content,
    required Map<String, String> Function(T) converter,
  }) => _CustomFieldsBody<T>(content, converter);
  
  /// Creates a CusomBody that will assign the result of `converter(content)` to 
  /// [Request.body]
  static CustomBody body<T>({
    required T content,
    required String Function(T) converter,
  }) => _CustomStringBody<T>(content, converter);

  /// Creates a CusomBody that will assign the result of `converter(content)` to
  /// [Request.bodyBytes]
  static CustomBody bytes<T>({
    required T content,
    required Uint8List Function(T) converter,
  }) => _CustomBytesBody<T>(content, converter);

  /// The piece of data held by this instance
  final Content content;

  /// Function that converts [Content] to [Target], where [Target] should be one of
  /// [Map<String, String>], [String] or [Uint8List]
  final Target Function(Content) converter;
}

class _CustomFieldsBody<T> extends CustomBody<T, Map<String, String>> {
  _CustomFieldsBody(T content, Map<String, String> Function(T) converter):
    super(content, converter);

  void setRequestBody(final Request request) => request.bodyFields = converter(content);
}

class _CustomStringBody<T> extends CustomBody<T, String> {
  _CustomStringBody(T content, String Function(T) converter):
    super(content, converter);

  void setRequestBody(final Request request) => request.body = converter(content);
}

class _CustomBytesBody<T> extends CustomBody<T, Uint8List> {
  _CustomBytesBody(T content, Uint8List Function(T) converter):
    super(content, converter);

  void setRequestBody(final Request request) => request.bodyBytes = converter(content);
}
