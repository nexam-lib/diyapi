class Method {
  const Method(this._value);
  
  final String _value;

  static const get = Method("GET");
  static const head = Method("HEAD");
  static const post = Method("POST");
  static const put = Method("PUT");
  static const patch = Method("PATCH");
  static const delete = Method("DELETE");
  static const connect = Method("CONNECT");
  static const option = Method("OPTIONS");
  static const trace = Method("TRACE");
  
  String toString() => _value;

  int get hashCode => _value.hashCode;
  
  bool operator==(Object o) =>
    o is Method &&
    _value == o._value;
}
