import 'dart:async';

import 'package:diy_api/src/http_service.dart';
import 'package:result_class/result_class.dart';
import 'package:http/http.dart';

import 'errors.dart';

class RequestInterceptor {
  const RequestInterceptor({
    required this.reaction,
    this.locking = false,
  });

  /// RequestInterceptor that cannot alter inspected requests.
  /// 
  /// Can be used to implement logging while ensuring that requests stay unaltered.
  RequestInterceptor.passthrough({
    required void Function(BaseRequest request) inspector,
  }): this(
    locking: false,
    reaction: (request, _) {
      inspector(request);
      return Result.ok(request);
    },
  );


  /// A function that should return either a [BaseRequest] that is gonna get sent
  /// normally or a [Result] that gets returned as if it was the actual response,
  /// skipping any subsequent [RequestInterceptor]s.
  /// 
  /// {@template diy_api.interceptors.reaction}
  /// 
  /// Note that [request] has already been initialized including default values from
  /// [BaseHttpService] so updating those values from within a [reaction] won't affect
  /// it. In that case one has to manually update the request itself before sending it.
  /// {@endtemplate}
  final FutureOr<Result<BaseRequest, Result<Response, ClientError>>> Function(BaseRequest request, HttpDelegate httpDelegate) reaction;

  /// Whether the [reaction] should block other requests until it's been completed
  final bool locking;

  String toString() => "RequestInterceptor: reaction: $reaction,\nlocking: $locking";
}

class ResponseInterceptor {
  const ResponseInterceptor({
    required this.reactIf,
    required this.reaction,
    this.locking = false,
  });

  /// ResponseInterceptor that cannot alter inspected requests.
  /// 
  /// Can be used to implement logging while ensuring that requests stay unaltered.
  ResponseInterceptor.passthrough({
    required void Function(BaseRequest request, Result<Response, ClientError> result) inspector,
  }): this(
    reactIf: (_, __) => true,
    reaction: (request, result, _) {
      inspector(request, result);
      return result;
    },
  );

  /// A function that should return true if [reaction] should be called, otherwise it
  /// should return false.
  final FutureOr<bool> Function(
    BaseRequest request,
    Result<Response, ClientError> result,
  ) reactIf;
  
  /// A function that for inspecting and optionally altering responses handled by
  /// [HttpService]
  /// 
  /// This function is only called if [reactIf] returns true for [request]. In that case
  /// it is called with the original request, the original result (if this is the first
  /// interceptor) / the previous result (if this is another interceptor) and an
  /// [HttpDelegate] that allows making HTTP requests.
  /// 
  /// {@macro diy_api.interceptors.reaction}
  final FutureOr<Result<Response, ClientError>> Function(
    BaseRequest request,
    Result<Response, ClientError> result,
    HttpDelegate httpDelegate,
  ) reaction;
  
  /// Whether the [reaction] should block other requests until it's been completed
  final bool locking;

  String toString() => "ResponseInterceptor: test: $reactIf,\nreaction: $reaction,\nlocking: $locking";
}
