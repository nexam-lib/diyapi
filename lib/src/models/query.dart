import 'package:diy_api/src/http_service.dart';

/// Convenience class for safely creating queries for HTTP requests
abstract class Query {
  /// Creates a Query from [queryParameters].
  /// 
  /// In Debug mode this asserts that [queryParameters.values] contains only Strings or
  /// Iterables of String, additionally it will throw a [TypeError] when used inside [HttpService.createRequest] or [HttpService.createMultipartRequest].
  factory Query.create(Map<String, dynamic> queryParameters) = _MapQuery.create;

  /// Creates a Query from [queryParameters].
  /// 
  /// Returns null if [queryParameters.values] contains values that are neither Strings
  /// or Iterables of String.
  static Query? tryCreate(Map<String, dynamic> queryParameters) =>
    _MapQuery.tryCreate(queryParameters);

  /// Creates a Query from [queryParameters] that can only have one value per key
  const factory Query.singleValues(Map<String, String> queryParameters) = _MapQuery.singleValues;

  /// Creates a Query from [queryParameters] that can only have multiple values per key
  /// 
  /// (Technically you can have Iterables with one item each, but that's not idiomatic)
  const factory Query.multipleValues(Map<String, Iterable<String>> queryParameters) = _MapQuery.multipleValues;

  /// Creates a Query from [query]
  const factory Query.string(String query) = _StringQuery;

  static const Query empty = _MapQuery._({});

  const Query._();

  Uri setInUri(Uri uri);
}

class _StringQuery extends Query {
  const _StringQuery(this._query): super._();

  final String _query;

  Uri setInUri(Uri uri) => uri.replace(query: _query);

  String toString() => "_StringQuery($_query)";
}

class _MapQuery extends Query {
  _MapQuery.create(this._queryParameters):
    assert(_queryParameters.values.every((v) => v is String || v is Iterable<String>)),
    super._();

  static _MapQuery? tryCreate(Map<String, dynamic> queryParameters) =>
    queryParameters.values.every((v) => v is String || v is Iterable<String>)
      ? _MapQuery._(queryParameters)
      : null;

  const _MapQuery.singleValues(Map<String, String> queryParameters):
    _queryParameters = queryParameters,
    super._();

  const _MapQuery.multipleValues(Map<String, Iterable<String>> queryParameters):
    _queryParameters = queryParameters,
    super._();

  const _MapQuery._(this._queryParameters): super._();

  final Map<String, dynamic> _queryParameters;

  Uri setInUri(Uri uri) => uri.replace(queryParameters: _queryParameters);

  String toString() => "_MapQuery($_queryParameters)";
}