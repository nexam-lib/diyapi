import 'dart:async';

import 'package:http/http.dart';

class ClientError implements Exception {
  const ClientError([this.message]);
  
  const factory ClientError.timeout(Duration? duration, [String? message])
    = TimeoutError;

  factory ClientError.fromTimeoutException(TimeoutException exception)
    = TimeoutError.from;

  factory ClientError.fromClientException(ClientException exception) = HttpError.from;

  const factory ClientError.http(Uri uri, [String? message]) = HttpError;

  final String? message;

  String toString() => "ClientError($message)";
}

class TimeoutError extends ClientError implements Exception {
  const TimeoutError(this.duration, [String? message]):
    super(message);
  
  TimeoutError.from(TimeoutException exception):
    duration = exception.duration,
    super(exception.message);
  
  final Duration? duration;

  String toString() => "TimeoutError:\nmessage: $message,\nduration: $duration";
}

class HttpError extends ClientError implements Exception {
  const HttpError(this.uri, [String? message]): super(message);

  HttpError.from(ClientException exception):
    uri = exception.uri,
    super(exception.message);

  final Uri? uri;

  String toString() => "HttpError:\nmessage: $message,\nURI: $uri";
}
