import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:diy_api/src/models/errors.dart';
import 'package:diy_api/src/models/interceptors.dart';
import 'package:diy_api/src/models/query.dart';
import 'package:result_class/result_class.dart';
import 'models/body.dart';
import 'models/method.dart';
import 'util.dart';

abstract class BaseHttpService {
  const BaseHttpService();

  /// Headers that are send along with every request
  Map<String, String> get headers;

  /// The maximum number of redirects to follow. Set to null to disable redirects.
  /// 
  /// Defaults to 5.
  int get maxRedirects => 5;

  /// Whether requests should keep connections persistent by default.
  /// 
  /// Defaults to true.
  bool get persistentConnection => true;

  /// Default encoding for [Request]s
  Encoding? get encoding;

  /// Creates a [Request] from [method] and [uri]
  /// 
  /// {@template diy_api.http_service.requestParameters}
  /// 
  /// Set [persistentConnection] to false to disable the internal persistent connections.
  /// 
  /// [followRedirects] and [maxRedirects] control whether and how many redirects the
  /// internal [BaseClient] should follow. When setting [followRedirects] to null, ensure
  /// that either of [maxRedirects] or [BaseHttpService.maxRedirects] is not null.
  /// Set [followRedirects] to null to fall back to [BaseHttpService.maxRedirects].
  /// 
  /// [followRedirects] and maxRedirects default to null.
  /// 
  /// Set [headers] to include headers in this request. [BaseHttpService.headers] are
  /// only added when the request is actually sent.
  /// {@endtemplate}
  /// 
  /// {@template diy_api.http_service.createRequest}
  /// 
  /// Set [body] to include a body in the request.
  /// 
  /// [encoding] is passed to [Request.encoding].
  /// 
  /// See also:
  ///  * [Query], for how to add a query to the [uri].
  ///  * [Body], for how to add a body to the request.
  ///  * [Request.encoding] for the [encoding] parameter.
  /// {@endtemplate}
  Request createRequest({
      required Method method,
      required Uri uri,
      bool? persistentConnection = true,
      bool? followRedirects,
      int? maxRedirects,
      Body? body,
      Query? query,
      Map<String, String>? headers,
      Encoding? encoding,
  }) {
    assert(followRedirects == null || maxRedirects != null,
        "If followRedirects is not null, maxRedirects has to be non-null, too.");
    final request = Request("$method", query?.setInUri(uri) ?? uri);
  
    request.headers.addAll(this.headers);
    if (headers != null)
      request.headers.addAll(headers);

    request.followRedirects = followRedirects ??
        ((maxRedirects != null && maxRedirects > 0) || this.maxRedirects > 0);
    request.maxRedirects = maxRedirects ?? this.maxRedirects;
    request.persistentConnection = persistentConnection ?? this.persistentConnection;
    encoding ??= this.encoding;
    if (encoding != null)
      request.encoding = encoding;
    body?.setRequestBody(request);
    
    return request;
  }

  /// Creates a [MultipartRequest] from [method] and [uri]
  /// 
  /// {@template diy_api.http_service.createMultipartRequest}
  /// 
  /// [fields] are the form fields of the [MultipartRequest].
  /// 
  /// [files] are the files to upload as part of this request.
  /// 
  /// {@macro diy_api.http_service.requestParameters}
  /// 
  /// See also:
  ///  * [Query], for how to add a query to the [uri].
  ///  * [MultipartFile]
  /// {@endtemplate}
  MultipartRequest createMultipartRequest({
      required Method method,
      required Uri uri,
      bool? persistentConnection = true,
      Map<String, String> fields = const {},
      List<MultipartFile> files = const [],
      bool? followRedirects,
      int? maxRedirects = 5,
      Query? query,
      Map<String, String>? headers,
  }) {
    assert(followRedirects == null || maxRedirects != null,
        "If followRedirects is not null, maxRedirects has to be non-null, too.");

    final request = MultipartRequest("$method", query?.setInUri(uri) ?? uri);
  
    request.headers.addAll(this.headers);
    if (headers != null)
      request.headers.addAll(headers);
    
    request.followRedirects = followRedirects ??
        ((maxRedirects != null && maxRedirects > 0) || this.maxRedirects > 0);
    request.maxRedirects = maxRedirects ?? this.maxRedirects;
    request.persistentConnection = persistentConnection ?? this.persistentConnection;
    request.fields.addAll(fields);
    request.files.addAll(files);
    
    return request;
  }

  /// Send a request
  Future<Result<Response, ClientError>> sendRequest(Request request, [Duration? timeout]);

  /// Send a request
  /// 
  /// {@macro diy_api.http_service.requestParameters}
  /// 
  /// {@macro diy_api.http_service.createRequest}
  Future<Result<Response, ClientError>> send({
    required Method method,
    required Uri uri,
    bool persistentConnection = true,
    bool? followRedirects,
    int? maxRedirects = 5,
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout,
    Encoding? encoding,
  });

  /// Send a [MultipartRequest]
  /// 
  /// {@macro diy_api.http_service.requestParameters}
  /// 
  /// {@macro diy_api.http_service.createMultipartRequest}
  Future<Result<Response, ClientError>> sendMultipart({
    required Method method,
    required Uri uri,
    bool persistentConnection = true,
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    bool? followRedirects,
    int? maxRedirects = 5,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout,
  });
}

/// To reduce boilerplate, inherit [ApiService] instead
class HttpService extends BaseHttpService {
  /// [requestInterceptors] and [responseInterceptors] can be used to validate and/or intercept requests and responses.
  /// 
  /// [headers] are HTTP headers that always get sent with requests. To send special headers with specific requests use the optional argument in the request methods
  HttpService({
    Map<String, String>? headers,
    List<RequestInterceptor>? requestInterceptors,
    List<ResponseInterceptor>? responseInterceptors,
    Client? client,
    this.timeout,
    this.persistentConnection = true,
    this.encoding,
  }):
    this._client = client ?? Client(),
    this.headers = headers ?? {},
    this.requestInterceptors = requestInterceptors ?? [],
    this.responseInterceptors = responseInterceptors ?? []
  {
    _subscription = _controller.stream.listen((task) => task.completer.complete(_handleTask(task)));
  }

  final Map<String, String> headers;
  final bool persistentConnection;
  final Encoding? encoding;

  /// Default timeout for all requests. Can be overridden individually
  Duration? timeout;

  /// Called in order for every request [Request] passed to [send] and [sendMultipart]
  final List<RequestInterceptor> requestInterceptors;

  /// Called in order for every response in [send] and [sendMultipart]
  final List<ResponseInterceptor> responseInterceptors;
  
  /// Closes the internal streams
  Future<void> dispose() => Future.wait([_controller.close(), _subscription.cancel()]);

  /// Whether the instance is currently able to handle requests
  bool get running => !_subscription.isPaused;

  /// Pauses the internal request queue, buffering them until resumed
  void pause() => _subscription.pause();
  
  /// Resumes handling requests
  void resume() => _subscription.resume();

  final Client _client;
  final StreamController<_Task> _controller = StreamController(sync: true);
  late final StreamSubscription<_Task> _subscription;
  late final HttpDelegate _delegate = HttpDelegate._(this);

  Future<Result<Response, ClientError>> send({
      required Method method,
      required Uri uri,
      bool persistentConnection = true,
      bool? followRedirects,
      int? maxRedirects = 5,
      Body? body,
      Map<String, String>? headers,
      Query? query,
      Duration? timeout,
      Encoding? encoding, }
  ) => _enqueue(
    timeout: timeout,
    withReaction: true,
    request: createRequest(
      method: method,
      uri: uri,
      persistentConnection: persistentConnection,
      body: body,
      headers: headers,
      query: query,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
      encoding: encoding,
    ),
  );
  
  /// Send a request, bypassing [requestInterceptors] and [responseInterceptors].
  /// 
  /// {@macro diy_api.http_service.requestParameters}
  /// 
  /// {@macro diy_api.http_service.createRequest}
  Future<Result<Response, ClientError>> sendWithoutInterceptors({
      required Method method,
      required Uri uri,
      bool persistentConnection = true,
      bool? followRedirects,
      int? maxRedirects = 5,
      Body? body,
      Map<String, String>? headers,
      Query? query,
      Duration? timeout,
      Encoding? encoding,
  }) => _enqueue(
    timeout: timeout,
    withReaction: false,
    request: createRequest(
      method: method,
      uri: uri,
      persistentConnection: persistentConnection,
      body: body,
      headers: headers,
      query: query,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
      encoding: encoding,
    ),
  );

  Future<Result<Response, ClientError>> sendMultipart({
      required Method method,
      required Uri uri,
      bool persistentConnection = true,
      Map<String, String> fields = const {},
      List<MultipartFile> files = const [],
      bool? followRedirects,
      int? maxRedirects = 5,
      Map<String, String>? headers,
      Query? query,
      Duration? timeout,
      Encoding? encoding,
  }) => _enqueue(
    timeout: timeout,
    withReaction: true,
    request: createMultipartRequest(
      method: method,
      uri: uri,
      persistentConnection: persistentConnection,
      fields: fields,
      files: files,
      headers: headers,
      query: query,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
    ),
  );

  /// Send a [MultipartRequest], bypassing [requestInterceptors] and
  /// [responseInterceptors].
  /// 
  /// {@macro diy_api.http_service.requestParameters}
  /// 
  /// {@macro diy_api.http_service.createMultipartRequest}
  Future<Result<Response, ClientError>> sendMultipartWithoutInterceptors({
      required Method method,
      required Uri uri,
      bool persistentConnection = true,
      Map<String, String> fields = const {},
      List<MultipartFile> files = const [],
      bool? followRedirects,
      int? maxRedirects = 5,
      Map<String, String>? headers,
      Query? query,
      Duration? timeout,
      Encoding? encoding,
  }) => _enqueue(
    timeout: timeout,
    withReaction: false,
    request: createMultipartRequest(
      method: method,
      uri: uri,
      persistentConnection: persistentConnection,
      fields: fields,
      files: files,
      headers: headers,
      query: query,
      followRedirects: followRedirects,
      maxRedirects: maxRedirects,
    ),
  );

  Future<Result<Response, ClientError>> sendRequest(
      BaseRequest request,
      [Duration? timeout,]
  ) => _enqueue(request: request, timeout: timeout, withReaction: true);

  Future<Result<Response, ClientError>> sendRequestWithoutInterceptors(
      BaseRequest request,
      [Duration? timeout,]
  ) => _enqueue(request: request, timeout: timeout, withReaction: false);

  Future<Result<Response, ClientError>> _enqueue({
      required BaseRequest request,
      required bool withReaction,
      required Duration? timeout,
  }) {
    final task = _Task(request, withReaction, timeout);
    _controller.add(task);
    return task.completer.future;
  }

  Future<Result<Response, ClientError>> _handleTask(_Task task) async {
    try {
      var request = task.request;
      request.headers.addAll(headers);
      if (task.withReaction) {
        final response = () async {
          final result = await _handleRequestInterceptors(request);
    
          if (result.isErr)
            return result.errorOr(const Result.err(ClientError()));
          else
            result.map((value) => request = value);
          
          var response = _client
            .send(request)
            .then((streamedResponse) => Response.fromStream(streamedResponse));
          
          final t = task.timeout ?? this.timeout;
          if (t != null)
            response = response.timeout(t);
          
          return Result<Response, ClientError>.ok(await response);
        }();
      
        return _handleResponseInterceptors(request, await response);
      } else {      
        var responseFuture = _client
          .send(request)
          .then((streamedResponse) => Response.fromStream(streamedResponse));
        
        final t = task.timeout ?? this.timeout;
        if (t != null)
          responseFuture = responseFuture.timeout(t);
        
        return Result.ok(await responseFuture);
      }
    } on TimeoutException catch (e) {
      return Result.err(TimeoutError.from(e));
    } on ClientException catch (e) {
      return Result.err(HttpError.from(e));
    } on RedirectException catch (e) {
      return Result.err(ClientError.http(task.request.url, "$e"));
    }
  }

  FutureOr<T> _callLocking<T>(FutureOr<T> Function() f) async {
    _subscription.pause();
    final res = await f();
    _subscription.resume();
    return res;
  }
  
  /// Returns a new request of the same type and with equal fields as [request].
  /// 
  /// This can be necessary if the new request
  static BaseRequest _copyAnyRequest(BaseRequest request) {
    if (request is Request)
      return Request(request.method, request.url)
        ..bodyBytes = request.bodyBytes
        ..encoding = request.encoding
        ..followRedirects = request.followRedirects
        ..maxRedirects = request.maxRedirects
        ..persistentConnection = request.persistentConnection
        ..headers.addAll(request.headers);
    else if (request is MultipartRequest)
      return MultipartRequest(request.method, request.url)
        ..followRedirects = request.followRedirects
        ..maxRedirects = request.maxRedirects
        ..persistentConnection = request.persistentConnection
        ..headers.addAll(request.headers)
        ..fields.addAll(request.fields)
        ..files.addAll(request.files);
    else
      throw UnsupportedError("Does not support copying request types other than Request and MultipartRequest");
  }

  Future<Result<BaseRequest, Result<Response, ClientError>>>
  _handleRequestInterceptors(BaseRequest request) async {
    request = _copyAnyRequest(request);
    var result = Result<BaseRequest, Result<Response, ClientError>>.ok(request);

    for (final interceptor in requestInterceptors) {
      result = interceptor.locking
        ? await _callLocking(() => interceptor.reaction(request, _delegate))
        : await interceptor.reaction(request, _delegate);
      
      if (result.isErr)
        return result;
    }

    return result;
  }
  
  Future<Result<Response, ClientError>> _handleResponseInterceptors(
      BaseRequest request,
      Result<Response, ClientError> result,
  ) {
    request = _copyAnyRequest(request);

    return responseInterceptors
      .asyncWhere((interceptor) => interceptor.reactIf(request, result))
      .asyncFold<Result<Response, ClientError>>(result, (result, interceptor) async =>
        interceptor.locking
          ? await _callLocking(() => interceptor.reaction(request, result, _delegate))
          : await interceptor.reaction(request, result, _delegate)
      );
  }
}

/// Passed to interceptors so they can bypass locks in the actual [HttpService]
class HttpDelegate extends BaseHttpService {
  const HttpDelegate._(this._parent);

  final HttpService _parent;

  /// Returns [HttpService.headers]
  Map<String, String> get headers => _parent.headers;

  bool get persistentConnection => _parent.persistentConnection;

  Encoding? get encoding => _parent.encoding;

  Future<Result<Response, ClientError>> sendRequest(
      BaseRequest request,
      [Duration? timeout,]
  ) => _parent.sendRequestWithoutInterceptors(request, timeout);

  Future<Result<Response, ClientError>> send({
      required Method method,
      required Uri uri,
      bool persistentConnection = true,
      bool? followRedirects,
      int? maxRedirects = 5,
      Body? body,
      Map<String, String>? headers,
      Query? query,
      Duration? timeout,
      Encoding? encoding,
  }) => _parent.sendWithoutInterceptors(
    method: method,
    uri: uri,
    persistentConnection: persistentConnection,
    body: body,
    headers: headers,
    query: query,
    followRedirects: followRedirects,
    maxRedirects: maxRedirects,
    timeout: timeout,
    encoding: encoding,
  );
  
  Future<Result<Response, ClientError>> sendMultipart({
      required Method method,
      required Uri uri,
      bool persistentConnection = true,
      Map<String, String> fields = const {},
      List<MultipartFile> files = const [],
      bool? followRedirects,
      int? maxRedirects = 5,
      Map<String, String>? headers,
      Query? query,
      Duration? timeout,
      Encoding? encoding,
  }) => _parent.sendMultipartWithoutInterceptors(
    method: method,
    uri: uri,
    persistentConnection: persistentConnection,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    followRedirects: followRedirects,
    maxRedirects: maxRedirects,
    timeout: timeout,
    encoding: encoding,
  );
}

class _Task {
  _Task(this.request, this.withReaction, this.timeout):
    completer = Completer();

  final BaseRequest request;
  final bool withReaction;
  final Duration? timeout;
  final Completer<Result<Response, ClientError>> completer;
}
