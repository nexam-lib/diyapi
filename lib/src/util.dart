import 'dart:async';

extension IterableExtension<E> on Iterable<E> {
  Stream<E> asyncWhere(FutureOr<bool> Function(E) test) async* {
    for (final e in this)
      if (await test(e))
        yield e;
  }

  Future<T> asyncFold<T>(T initialValue, Future<T> Function(T, E) combine) async {
    var value = initialValue;
    for (final e in this) {
      value = await combine(value, e);
    }
    return value;
  }
}

extension StreamExtension<E> on Stream<E> {
  Future<T> asyncFold<T>(T initialValue, Future<T> Function(T, E) combine) async {
    T value = initialValue;
    await for (final e in this)
      value = await combine(value, e);
    
    return value;
  }
}
