import 'diy_api.dart';
import 'models/interceptors.dart';

/// A class that can be used to define an API class with which to make requests to some
/// backend.
class ApiDelegate
  extends DiyApi 
  with
    GetHeadersMixin,
    GetRequestInterceptorsMixin,
    GetResponseInterceptorsMixin,
    SendMixin,
    GetMixin,
    HeadMixin,
    PostMixin,
    PutMixin,
    PatchMixin,
    DeleteMixin,
    ConnectMixin,
    OptionMixin,
    TraceMixin,
    GetMultipartMixin,
    HeadMultipartMixin,
    PostMultipartMixin,
    PutMultipartMixin,
    PatchMultipartMixin,
    DeleteMultipartMixin,
    ConnectMultipartMixin,
    OptionMultipartMixin,
    TraceMultipartMixin
{
  ApiDelegate({
    required Uri path,
    required Duration timeout,
    List<RequestInterceptor>? requestInterceptors,
    List<ResponseInterceptor>? responseInterceptors,
    Map<String, String>? headers,
  }): super(
    path: path,
    timeout: timeout,
    requestInterceptors: requestInterceptors,
    responseInterceptors: responseInterceptors,
    headers: headers,
  );
}

abstract class ApiDelegateChild {
  const ApiDelegateChild(this.baseUrl);

  /// The base URL of this API
  final Uri baseUrl;

  /// This childs path segment. 
  String get segment;

  /// Generates a [Uri] from 
  Uri makeUrl(String endpoint) => baseUrl
    .replace(path: "${baseUrl.path}/$segment/$endpoint");
}
