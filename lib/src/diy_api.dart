import 'dart:async';
import 'dart:io';

import 'package:http/http.dart';
import 'package:diy_api/src/http_service.dart';
import 'package:diy_api/src/models/interceptors.dart';
import 'package:diy_api/src/models/method.dart';
import 'package:result_class/result_class.dart';

import 'models/body.dart';
import 'models/errors.dart';
import 'models/query.dart';

/// Service for making type safe requests for and/or with predefined models.
/// 
/// To use it, extend [DiyApi] and mix in one or more Mixins defined below to define
/// how your APIs interface should look like with little boilerplate code.
/// 
/// ```dart
///   class MyApi extends ApiService
///       with GetHeadersMixin, GetMixin, PostMixin
///   {
///     MyApi():
///       super(
///         path: Uri.parse("https://example.com/api"),
///         timeout: Duration(seconds: 5),
///       );
///   
///     late final MyApiUser user = MyApiUser(this);
///   }
///   
///   class MyApiUser extends ApiServiceChild with GetMixin {
///     MyApiUser(ApiServiceNode parent):
///       super("user", parent);
///
///     /// Makes a GET request to http://example.com/api/user/{id}
///     Future<Result<Response, Error>> detail(int id) => get(pathWith("$id"));
///   }
/// ```
/// 
/// See also:
///   * [ApiServiceDelegate], which can be made a private member of your API service to
///     encapsulate its methods.
abstract class DiyApi extends DiyApiNode {
  /// Creates an ApiService
  /// 
  /// [path] is what the [segment] argument of the request methods get appended to.
  /// Usually has the form of `https://example.com/api`
  /// 
  /// The [timeout] controls after which [Duration] requests should time out by default.
  /// 
  /// [requestInterceptors] and [responseInterceptors] can be used to validate and/or intercept requests and responses.
  /// 
  /// [headers] are HTTP headers that always get sent with requests. To send special headers with specific requests use the optional argument in the request methods
  /// 
  DiyApi({
    required Uri path,
    required Duration timeout,
    List<RequestInterceptor>? requestInterceptors,
    List<ResponseInterceptor>? responseInterceptors,
    Map<String, String>? headers,
  }):
    _httpService = HttpService(
      headers: headers,
      requestInterceptors: requestInterceptors,
      responseInterceptors: responseInterceptors,
      timeout: timeout,
    ),
    super(path);
  
  final HttpService _httpService;

  /// API host. Requests prepend their [segment] with this
  Uri get url => super.url;

  /// Default timeout
  Duration? get timeout => _httpService.timeout;

  String get segment => "$url";

  DiyApi get _root => this;
  DiyApiNode? get _parent => null;

}

abstract class DiyApiNode {
  DiyApiNode(this.url);

  /// The root [DiyApi]s path
  late final Uri baseUrl = _root.url;

  /// The path to this node.
  /// 
  /// Consider a base [Uri] of `https://example.com/api`. Then a child with the [segment]
  /// "user" will have a [url] of `https://example.com/api/user`
  final Uri url;

  /// This nodes path segment. Note that this doesn't necessarily correspond to
  /// [Uri.pathSegments]
  String get segment;

  /// List of all segments of this nodes ancestors starting with the root node.
  /// 
  /// Note that this doesn't necessarily correspond to [Uri.pathSegments]
  late final List<String> segments = [...?_parent?.segments, segment];

  /// Constructs a query from [url], [segment] and [query]
  Uri makeUrl(String segment) => url.replace(path: "${url.path}$segment");

  DiyApi get _root;
  DiyApiNode? get _parent;
}

abstract class DiyApiChild extends DiyApiNode {
  DiyApiChild(this.segment, this._parent):
    _root = (() {
      if (_parent is DiyApi)
        return _parent;
      else if (_parent is DiyApiChild)
        return _parent._root;
      else
        throw StateError("There should always be one ApiServiceRoot node");
    }()),
    super(Uri.parse(_parent.segments.followedBy([segment, ""]).join("/")));

  final String segment;

  final DiyApiNode _parent;
  final DiyApi _root;
}

/// Mixin that provides a public getter for [headers]
mixin GetHeadersMixin on DiyApiNode {
  /// Headers that are send along with every [Request]
  Map<String, String> get headers => _root._httpService.headers;
}

/// Mixin that provides a public getter for [requestInterceptors]
mixin GetRequestInterceptorsMixin on DiyApiNode {
  /// These are called in order for every [Request] passed to [BaseHttpService.send]
  List<RequestInterceptor> get requestInterceptors =>
    _root._httpService.requestInterceptors;
}

/// Mixin that provides a public getter for [responseInterceptors]
mixin GetResponseInterceptorsMixin on DiyApiNode {
  /// These are called in order for every [Response] in [BaseHttpService.send]
  List<ResponseInterceptor> get responseInterceptors =>
    _root._httpService.responseInterceptors;
}

/// Mixin on [DiyApiNode] that provides methods for managing a `Bearer` token
mixin BearerMixin on DiyApiNode {
  /// Remove the `Bearer` token from the headers
  void removeBearer() =>
    _root._httpService.headers.remove(HttpHeaders.authorizationHeader);

  /// If [token] is null, removes the `Bearer` token, otherwise sets it to [token]
  set bearer(String? token) => token != null
    ? _root._httpService.headers[HttpHeaders.authorizationHeader] = "Bearer $token"
    : removeBearer();

  /// Get the `Bearer` token from the headers
  String? get bearer {
    final b = _root._httpService.headers[HttpHeaders.authorizationHeader];
    return b != null
      ? RegExp(r"Bearer (.*)").firstMatch(b)?.group(1)
      : b;
  }
}

/// Mixin on [DiyApiNode] that implements [send]
mixin SendMixin on DiyApiNode { 
  /// Send a user defined request with the given parameters
  Future<Result<Response, ClientError>> send(
    Method method,
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: method,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [get]
mixin GetMixin on DiyApiNode { 
  /// Send a get request with the given parameters
  Future<Result<Response, ClientError>> get(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.get,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [head]
mixin HeadMixin on DiyApiNode { 
  /// Send a head request with the given parameters
  Future<Result<Response, ClientError>> head(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.head,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [post]
mixin PostMixin on DiyApiNode { 
  /// Send a post request with the given parameters
  Future<Result<Response, ClientError>> post(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.post,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [put]
mixin PutMixin on DiyApiNode { 
  /// Send a put request with the given parameters
  Future<Result<Response, ClientError>> put(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.put,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [patch]
mixin PatchMixin on DiyApiNode { 
  /// Send a patch request with the given parameters
  Future<Result<Response, ClientError>> patch(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.patch,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [delete]
mixin DeleteMixin on DiyApiNode { 
  /// Send a delete request with the given parameters
  Future<Result<Response, ClientError>> delete(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.delete,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [connect]
mixin ConnectMixin on DiyApiNode { 
  /// Send a connect request with the given parameters
  Future<Result<Response, ClientError>> connect(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.connect,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [option]
mixin OptionMixin on DiyApiNode { 
  /// Send a option request with the given parameters
  Future<Result<Response, ClientError>> option(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.option,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [trace]
mixin TraceMixin on DiyApiNode { 
  /// Send a trace request with the given parameters
  Future<Result<Response, ClientError>> trace(
    Uri uri, {
    Body? body,
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.send(
    method: Method.trace,
    uri: uri,
    body: body,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [getMultipart]
mixin GetMultipartMixin on DiyApiNode { 
  /// Send a get request with the given parameters
  Future<Result<Response, ClientError>> getMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.get,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [headMultipart]
mixin HeadMultipartMixin on DiyApiNode { 
  /// Send a head request with the given parameters
  Future<Result<Response, ClientError>> headMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.head,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [postMultipart]
mixin PostMultipartMixin on DiyApiNode { 
  /// Send a post request with the given parameters
  Future<Result<Response, ClientError>> postMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.post,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}

/// Mixin on [DiyApiNode] that implements [putMultipart]
mixin PutMultipartMixin on DiyApiNode { 
  /// Send a put request with the given parameters
  Future<Result<Response, ClientError>> putMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.put,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}


/// Mixin on [DiyApiNode] that implements [patchMultipart]
mixin PatchMultipartMixin on DiyApiNode { 
  /// Send a patch request with the given parameters
  Future<Result<Response, ClientError>> patchMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.patch,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}


/// Mixin on [DiyApiNode] that implements [deleteMultipart]
mixin DeleteMultipartMixin on DiyApiNode { 
  /// Send a delete request with the given parameters
  Future<Result<Response, ClientError>> deleteMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.delete,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}


/// Mixin on [DiyApiNode] that implements [connectMultipart]
mixin ConnectMultipartMixin on DiyApiNode { 
  /// Send a connect request with the given parameters
  Future<Result<Response, ClientError>> connectMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.connect,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}


/// Mixin on [DiyApiNode] that implements [optionMultipart]
mixin OptionMultipartMixin on DiyApiNode { 
  /// Send a option request with the given parameters
  Future<Result<Response, ClientError>> optionMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.option,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}


/// Mixin on [DiyApiNode] that implements [traceMultipart]
mixin TraceMultipartMixin on DiyApiNode { 
  /// Send a trace request with the given parameters
  Future<Result<Response, ClientError>> traceMultipart(
    Uri uri, {
    Map<String, String> fields = const {},
    List<MultipartFile> files = const [],
    Map<String, String>? headers,
    Query? query,
    Duration? timeout, }
  ) => _root._httpService.sendMultipart(
    method: Method.trace,
    uri: uri,
    fields: fields,
    files: files,
    headers: headers,
    query: query,
    timeout: timeout,
  );
}
