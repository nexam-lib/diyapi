/// Provides ApiService, which you can use to implement intuitive API services for your
/// app
/// 
/// To use, import `package:diy_api/api_service.dart`.
/// 
/// See also:
///
///  * [HttpService], used internally by [ApiService]
library api_service;

export 'package:http/src/response.dart';
export 'package:result_class/result_class.dart';

export 'package:diy_api/src/api_delegate.dart';
export 'package:diy_api/src/diy_api.dart';
export 'package:diy_api/src/http_service.dart';
export 'package:diy_api/src/models/body.dart';
export 'package:diy_api/src/models/errors.dart';
export 'package:diy_api/src/models/interceptors.dart';
export 'package:diy_api/src/models/method.dart';
export 'package:diy_api/src/models/query.dart';
