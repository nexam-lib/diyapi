# diy_api

A library that provides services for making HTTP requests. 

## HttpService

This is a wrapper around `http.Client` that simplifies sending requests. Also it relies on `Result` to return errors instead of exceptions to make error handling explicit. It has [RequestInterceptors] and [ResponseInterceptors], with which one can define ways of reacting to certain requests or inject a logger.

## Intuitive interfaces for your API service 

I like to to create tree-like interfaces for my API services that mirror the way they are built on the backend side. This can be seen as kind of like namespaces, where your requests look like this:

```dart
  final photoDetails = await api.photos.details(photoId: 42);
```

And while this is obviously possible with any kind of implementation. This library hopefully makes this a little bit easier.

## DiyApi

[DiyApi] gives you fine grained control over what kind of requests you API exposes having all actual implementations in mixins. Advantages compared to [ApiDelegate] are possibly less boilerplate as well as better composability. The main disadvantage is that by necessity more fields are public.

```dart
import 'package:diy_api/diy_api.dart';

class MyApi extends DiyApi
    with GetHeadersMixin, GetMixin, PostMixin
{
  MyApi():
    super(
      path: Uri.parse("https://myapi.com/api"),
      timeout: Duration(seconds: 5),
    );

  late final MyApiUser user = MyApiUser(this);
}

class MyApiUser extends DiyApiChild with GetMixin {
  MyApiUser(DiyApiNode parent):
    super("user", parent);

  late final settings = MyApiUserSettings(this);

  Future<Result<Response, ClientError>> detail({int userId}) => get(makeUrl("userDetail"));
}

class MyApiUserSettings extends DiyApiChild with PostMixin {
  MyApiUserSettings(this._parent) : super("settings", _parent);

  final DiyApiNode _parent;

  /// Sends a POST request to https://myapi.com/api/user/{userId}/settings
  Future<Result<Response, ClientError>> change(int userId, UserSettings userSettings) =>
    post(
      baseUrl.replace(path: "${baseUrl.path}/${_parent.segment}/$userId/$segment"),
      body: Body.body(json.encode(userSettings.toJson())),
    );
}

class UserSettings {
  ...
}

void useApi() async {
  final api = MyApi();
  final detailResult = await api.user.detail(42);
  detailResult.mapOrElse(
    (response) => print(response),
    (error) => print("Error: $error"),
  );
}

```

## ApiDelegate

Alternative to [DiyApi]. It is meant to simply be made a member of an actual API service class. The advantage compared to [DiyApi] is that users of this class have more control over fields' visibility. On the other hand composition doesn't work as well due to its simpler design.

```dart
import 'package:diy_api/diy_api.dart';

class MyApi {
  MyApi();

  late final user = MyApiUser(_apiDelegate);

  final _apiDelegate = ApiDelegate(
    path: Uri.parse("https://myapi.com/api"),
    timeout: const Duration(seconds: 5),
  );
}

class MyApiUser extends ApiDelegateChild {
  MyApiUser(this._delegate): super(_delegate.url);

  String get segment => "user";

  late final settings = MyApiUserSettings(_delegate, "$segment/settings");

  /// Sends a GET request to htto://myapi.com/api/{segment}/{userId}, where in this
  /// example [segment] is "user".
  Future<Result<Response, ClientError>> info(int userId) => _delegate.get(
    makeUrl("$userId"),
  );

  final ApiDelegate _delegate;
}

class MyApiUserSettings extends ApiDelegateChild {
  MyApiUserSettings(this._delegate, this.parentSegment):
    super(_delegate.url);

  static const settingsSegment = "settings";

  final String parentSegment;
  String get segment => "$parentSegment/$settingsSegment";

  /// Sends a POST request to https://myapi.com/api/user/{userId}/settings
  Future<Result<Response, ClientError>> change(int userId, UserSettings userSettings) =>
    _delegate.post(
      baseUrl.replace(path: "${baseUrl.path}/$parentSegment/$userId/$settingsSegment"),
      body: Body.body(json.encode(userSettings.toJson())),
    );

  final ApiDelegate _delegate;
}

class UserSettings {
  ...
}

void useApi() async {
  final api = MyApi();
  final settingsResult = await api.user.settings
    .change(42, UserSettings(name: "MyNewName"));
  settingsResult.mapOrElse(
    (response) => print(response),
    (error) => print("Error: $error"),
  );
}
```
